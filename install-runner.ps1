<#
iex (iwr 'https://gitlab.com/par.hultman/runner-install-windows/raw/master/install-runner.ps1' -UseBasicParsing).content
#>

md ~\runner
cd ~\runner

# Setup answers for git
@'
[Setup]
Lang=default
Dir=C:\Program Files\Git
Group=Git
NoIcons=1
SetupType=default
Components=ext,ext\shellhere,ext\guihere,gitlfs,assoc,assoc_sh
Tasks=
EditorOption=Nano
CustomEditorPath=
PathOption=Cmd
SSHOption=OpenSSH
TortoiseOption=false
CURLOption=WinSSL
CRLFOption=CRLFAlways
BashTerminalOption=MinTTY
PerformanceTweaksFSCache=Enabled
UseCredentialManager=Enabled
EnableSymlinks=Disabled
EnableBuiltinInteractiveAdd=Disabled
'@ | set-content  ~\runner\git_setup.inf



$gitUrl ='https://github.com/git-for-windows/git/releases/download/v2.23.0.windows.1/Git-2.23.0-64-bit.exe'
$progressPreference = 'silentlyContinue'
Invoke-WebRequest -Uri $gitUrl -OutFile Git-2.23.0-64-bit.exe -UseBasicParsing


&  .\Git-2.23.0-64-bit.exe /loadinf="git_setup.inf" /Silent


md c:\gitlab-runner
cd c:\gitlab-runner
$runnerUrl ='https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe'
Invoke-WebRequest -Uri $runnerUrl -OutFile gitlab-runner.exe -UseBasicParsing

&  .\gitlab-runner.exe install
&  .\gitlab-runner.exe register --url https://gitlab.com --tag-list windows,powershell -name test-runner --executor shell --registration-token abce
&  .\gitlab-runner.exe start

 


